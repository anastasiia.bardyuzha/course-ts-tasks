# Tasks

You have 9 tasks (src/ts/task*.ts) for improvement your knowledge of ts.

Please check your answers here:
https://www.typescriptlang.org/play
